## teensy-make ##
Minimal Teensyduino Toolchain using Makefile and Command Line Teensy Loader 

[adapted from apmorton](https://github.com/apmorton/teensy-template)

[using teensy_loader_cli](https://github.com/PaulStoffregen/teensy_loader_cli)

#### install  ####
```
$ git clone https://gitlab.com/qynn/teensy-make/git ~/Tnsy/teensy-make
$ sudo cp teensy-make/tools/49-teensy.rules /etc/udev/rules.d
```
#### use  ####
- copy additional libraries to `lib` folder
- edit paths in `Makefile`
- select/edit Teensy MCU (e.g. 3.2, LC) [more here](https://github.com/PaulStoffregen/teensy_loader_cli/blob/master/README.md)
- make upload (`SPC c c`)
 

